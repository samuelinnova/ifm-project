<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCabecerasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cabeceras', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('ubicacion')->default('');
            $table->string('cliente')->default('');
            $table->string('equipo')->default('');
            $table->longtext('desc_elemento');
            $table->integer('id_instalacion')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cabeceras');
    }
}
