<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlarmasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alarmas', function (Blueprint $table) {
            $table->integer('id');
            $table->timestamps();
            $table->integer('id_elemento')->unsigned()->default(0);
            $table->integer('id_instalacion')->unsigned()->default(0);
            $table->integer('idalarma');
            $table->datetime('startdate');
            $table->longtext('descripcion_alarma');
            $table->double('valor')->default(0);
            $table->integer('estado')->default(0);
            $table->integer('error')->default(0);
            $table->double('se01_a_peak_valor')->default(0);
            $table->double('se02_a_peak_valor')->default(0);
            $table->double('se03_a_peak_valor')->default(0);
            $table->double('se04_a_peak_valor')->default(0);
            $table->double('se02_des_valor')->default(0);
            $table->double('se04_des_valor')->default(0);
            $table->double('se02_cavit_valor')->default(0);
            $table->double('se02_desalineamiento_valor')->default(0);
            $table->double('se04_desalineamiento_valor')->default(0);
            $table->double('se01_v_rms_valor')->default(0);
            $table->double('se01_a_rms_valor')->default(0);
            $table->double('se02_v_rms_valor')->default(0);
            $table->double('se02_a_rms_valor')->default(0);
            $table->double('se03_v_rms_valor')->default(0);
            $table->double('se03_a_rms_valor')->default(0);
            $table->double('se04_v_rms_valor')->default(0);
            $table->double('se04_a_rms_valor')->default(0);
            $table->double('se04_la_valor')->default(0);
            $table->double('se03_loa_valor')->default(0);
            $table->datetime('fecha_cerrada');
            $table->integer('estado_alarma')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alarmas');
    }
}
