<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alarma extends Model
{
    //
    protected $fillable=['id_elemento','idalarma','id_instalacion','startdate','descripcion_alarma','valor','estado','error','se01_a_peak_valor','se02_a_peak_valor','se03_a_peak_valor','se04_a_peak_valor','se02_des_valor','se04_des_valor','se02_cavit_valor','se02_desalineamiento_valor','se04_desalineamiento_valor','se01_v_rms_valor','se01_a_rms_valor','se02_v_rms_valor','se02_a_rms_valor','se03_v_rms_valor','se03_a_rms_valor','se04_v_rms_valor','se04_a_rms_valor','se04_la_valor','se03_loa_valor','fecha_cerrada','estado'];
}
