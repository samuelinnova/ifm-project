<?php

namespace App\Exports;

use App\Alarma;
use Maatwebsite\Excel\Concerns\FromCollection;

class AlarmasExport implements FromCollection 
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        
        return $this->alarmas->all();
    }
}
