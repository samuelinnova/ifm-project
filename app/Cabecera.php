<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cabecera extends Model
{
    //
    protected $fillable=['ubicacion','cliente','equipo','desc_elemento','id_instalacion'];
}
