<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;

class MailAlarma extends Mailable
{
    use Queueable, SerializesModels;

    public $variable;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($variable)
    {
        //
        $this->variable = DB::table('alarmas')->where('estado_alarma',1)->orderby('created_at','DESC')->first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view("mails.alarma")
            ->from("victormorales1999@gmail.com")
            ->subject("Nueva Alarma");
    }
}
