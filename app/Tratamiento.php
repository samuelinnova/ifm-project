<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tratamiento extends Model
{
    //
    protected $fillable=['id_alarma','operario','fecha','accion','tratamiento'];
}
