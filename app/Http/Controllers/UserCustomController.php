<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Hash;

class UserCustomController extends Controller
{

    public function index($id)
    {
        if(auth()->user()->rol == 1){
        $user = User::find($id);
        return view('admin.user_edit')->with('user',$user);
    }
    else{
        return rediect('/preparacion');
    }
    }
    public function showAll(){
        if(auth()->user()->rol == 1){
            $usuarios = User::all();

            return view('admin.users_list')->with('usuarios', $usuarios);
        }
        else{
            return rediect('/preparacion');
        }
    }
    public function create()
    {
        $usuarios = User::all();
      
        if(auth()->user()->rol == 0){
            return redirect('/home');
        }
        if(auth()->user()->rol == 1){
            return view('admin.user_create')->with('usuarios',$usuarios);
        }
        
    }
    public function create_web(Request $request)
    {
        $usuarios = User::all();
        if(auth()->user()->rol == 1){
       if($request->email != ''){
            $email = $request->email;
        }else{
            $email="";
        }
        if($request->bloqueado != ''){
            $bloqueado = $request->bloqueado;
        }else{
            $bloqueado=false;
        }
        if($request->name != ''){
            $name = $request->name;
        }else{
            $name="0";
        }
        if($request->password != ''){
            $password=Hash::make($request->password);
        }else{
            $password = 0;
        }
        if($request->apellido != ''){
            $apellido = $request->apellido;
        }else{
            $apellido ="";
        }
        if($request->grupo != ''){
            $grupo = $request->grupo;
        }else{
            $grupo ="";
        }
        if($request->rol != ''){
            $rol = $request->rol;
        }else{
            $rol ="0";
        }
        
        
        User::create(['name' => $name, 'apellido'=>$apellido,'email'=>$email, 'bloqueado'=>$bloqueado,  'password'=>$password,'grupo_de_trabajo'=>$grupo,'rol'=>$rol]);
        return redirect('admin/users_list');
    }
    else{
        return rediect('/preparacion');
    }
    }

    public function store(Request $request)
    {
        //
    }
    public function show_web(){
    
        if(auth()->user()->rol == 0){
           
                
                return view('cambiar_password');
            
        }
        if(auth()->user()->rol == 1){
            
                
                return view('cambiar_password');
            
        }
    
    }
 

  
    function update(Request $request, $id){
        $user = User::find($request->id);
        $user->name = $request->name;
        $user->apellido = $request->apellido;
        $user->email = $request->email;
        if($request->password != null){
            $user->password=Hash::make($request->password) ;
        }
        $user->grupo_de_trabajo = $request->grupo;
        $user->save();
       // DB::table('users')->where('id', $id)->update(['name' => $request->name, 'apellido'=>$request->apellido,'email'=>$request->email,  'password'=>Hash::make($request->password),'codigo'=>$request->codigo]);
        // echo $request;
         return redirect('admin/users_list'); 
    }
    function update_web(Request $request, $id){

        $user = User::find($id);
        // echo $request;
        // $user->update($request->all());
        if($request->password!=null){
            $password=Hash::make($request->password);
        DB::table('users')->where('id', $id)->update(['password'=>$password]);
        }
        // echo $request;

        if($user){
            return redirect('/home');
        }else{
            return redirect('/home');
        }
    }

    public function toggleDisable(Request $request)
    {

        $option = $request->disabled;
        $user = $request->user;
        DB::table('users')->where('id', $user)->update(['bloqueado' => $option]);

        $usuarios = User::all();
        return redirect('admin/users_list');
    }

    public function destroy($id)
    {
        // funciona de puta madre
        $user = User::find($id);
        $usuarios = User::all();
        $user->delete();
        return redirect('admin/users_list');
    }
    public function home($id){
        return view('home')->with('usuario', $usuario);
    }
}
