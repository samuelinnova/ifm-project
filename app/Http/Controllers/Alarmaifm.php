<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Exports\AlarmasExport;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Alarma;
use App\Evento;
use App\Estado;
use App\Elemento;
use Carbon\Carbon;
use App\User;
class Alarmaifm extends Controller 
{
    public function getTest()
    {
        $alarmasbd = Alarma::all();
        $db_ext = \DB::connection('comments');
        $alarmas = DB::table('alarmas')->orderby('id','DESC')->take(10)->get();
        $alarmaid = DB::table('alarmas')->where('id',1)->first();
        $estados = Estado::all();
        foreach($alarmasbd as $alarma){
            if($alarma->estado_alarma == 0){
                $estadoalarma=Alarma::where('id', $alarma->id)->update( ['estado_alarma'=> 1 ]);
            }
        }
        return view('home')->with('alarmas',$alarmas)->with('alarmasbd',$alarmasbd);
    }
    public function mostrarTabla($id)
    {
        $alarmasbd = Alarma::all();
        $db_ext = \DB::connection('comments');
        $alarmas = DB::table('alarmas')->orderby('id','DESC')->take(10)->get();
        $alarmaid = DB::table('alarmas')->where('id',$id)->first();
        return view('home')->with('alarmas',$alarmas)->with('alarmaid',$alarmaid)->with('alarmasbd',$alarmasbd);
    }
    public function busqueda(request $request)
    {
        $alarmasbd = Alarma::all();
        $db_ext = \DB::connection('comments');
        if($request->datepicker == null && $request->alarma == 'todas' && $request->tipo == 'Prealarma'){
                    $alarmas = DB::table('alarmas')->wherein('idalarma',[1,11,7,3,13])->orderby('id','DESC')->take(1000)->get();
        }
        if($request->datepicker == null && $request->alarma == 'todas' && $request->tipo == 'Alarma'){
            $alarmas = DB::table('alarmas')->wherein('idalarma',[4,12])->orderby('id','DESC')->take(1000)->get();
        }
        if($request->tipo == 'todas' && $request->datepicker != null && $request->alarma != 'todas'){
            $alarmas =DB::table('alarmas')->where('idalarma',$request->alarma)->whereBetween('startdate',[$request->datepicker,$request->datepicker2])->orderby('id','DESC')->take(1000)->get();
        }
        if($request->alarma == 'todas' && $request->tipo == 'Prealarma' && $request->datepicker != null  ){
             $alarmas = DB::table('alarmas')->wherein('idalarma',[1,11,7,3,13])->whereBetween('startdate',[$request->datepicker,$request->datepicker2])->orderby('id','DESC')->take(1000)->get();
        } if($request->alarma == 'todas' && $request->tipo == 'Alarma' && $request->datepicker != null){
            $alarmas = DB::table('alarmas')->wherein('idalarma',[4,12])->whereBetween('startdate',[$request->datepicker,$request->datepicker2])->orderby('id','DESC')->take(1000)->get();
        }
        if($request->alarma == 'todas' && $request->tipo == 'todas' &&  $request->datepicker == null){
            $alarmas = Alarma::all();
        }
        if($request->alarma == 'todas' && $request->tipo == 'todas' && $request->datepicker != null ){
            $alarmas = DB::table('alarmas')->whereBetween('startdate',[$request->datepicker,$request->datepicker2])->orderby('id','DESC')->take(1000)->get();
        }
        if($request->tipo == 'todas' && $request->datepicker == null && $request->alarma != 'todas'){
            $alarmas =DB::table('alarmas')->where('idalarma',$request->alarma)->orderby('id','DESC')->take(1000)->get();
        }
        if($request->alarma != 'todas' && $request->tipo == 'Prealarma' && $request->datepicker != null ){
            $alarmas = DB::table('alarmas')->where('idalarma',$request->alarma)->wherein('idalarma',[1,11,7,3,13])->whereBetween('startdate',[$request->datepicker,$request->datepicker2])->orderby('id','DESC')->take(1000)->get();
        }
        if($request->alarma != 'todas' && $request->tipo == 'Alarma' && $request->datepicker != null ){
            $alarmas = DB::table('alarmas')->where('idalarma',$request->alarma)->wherein('idalarma',[4,16])->whereBetween('startdate',[$request->datepicker,$request->datepicker2])->orderby('id','DESC')->take(1000)->get();
        }
        $alarmaid = DB::table('alarmas')->where('id',1)->first();
        return view('home')->with('alarmas',$alarmas)->with('alarmaid',$alarmaid)->with('request',$request)->with('alarmasbd',$alarmasbd);
    }
    public function historialincidencias()
    {
        $alarmasbd = Alarma::all();
        $db_ext = \DB::connection('comments');
        $alarmas = DB::table('alarmas')->orderby('id','DESC')->take(10)->get();
        $estados = Estado::all();
        $eventos = Evento::all();
        $users = User::all();
        return view('admin.historial_incidencias')->with('alarmas',$alarmas)->with('alarmasbd',$alarmasbd)->with('eventos',$eventos)->with('usuarios',$users);
    }
    public function historialincidencias_busqueda(request $request)
    {
        $alarmasbd = Alarma::all();
        $db_ext = \DB::connection('comments');
        $estados = Estado::all();
        $eventos = Evento::all();
        $users = User::all();
        if($request->datepicker == null && $request->alarma == 'todas' && $request->tipo == 'Prealarma'){
             $alarmas = DB::table('alarmas')->wherein('idalarma',[1,11,7,3,13])->orderby('id','DESC')->take(1000)->get();
        }
        if($request->datepicker == null && $request->alarma == 'todas' && $request->tipo == 'Alarma'){
            $alarmas = DB::table('alarmas')->wherein('idalarma',[4,12])->orderby('id','DESC')->take(1000)->get();
        }
        if($request->tipo == 'todas' && $request->datepicker != null && $request->alarma != 'todas'){
            $alarmas =DB::table('alarmas')->where('idalarma',$request->alarma)->whereBetween('startdate',[$request->datepicker,$request->datepicker2])->orderby('id','DESC')->take(1000)->get();
        }
        if($request->alarma == 'todas' && $request->tipo == 'Prealarma' && $request->datepicker != null  ){
             $alarmas = DB::table('alarmas')->wherein('idalarma',[1,11,7,3,13])->whereBetween('startdate',[$request->datepicker,$request->datepicker2])->orderby('id','DESC')->take(1000)->get();
        } if($request->alarma == 'todas' && $request->tipo == 'Alarma' && $request->datepicker != null){
            $alarmas = DB::table('alarmas')->wherein('idalarma',[4,12])->whereBetween('startdate',[$request->datepicker,$request->datepicker2])->orderby('id','DESC')->take(1000)->get();
        }
        if($request->alarma == 'todas' && $request->tipo == 'todas' &&  $request->datepicker == null){
            $alarmas = Alarma::all();
        }
        if($request->alarma == 'todas' && $request->tipo == 'todas' && $request->datepicker != null ){
            $alarmas = DB::table('alarmas')->whereBetween('startdate',[$request->datepicker,$request->datepicker2])->orderby('id','DESC')->take(1000)->get();
        }
        if($request->tipo == 'todas' && $request->datepicker == null && $request->alarma != 'todas'){
            $alarmas =DB::table('alarmas')->where('idalarma',$request->alarma)->orderby('id','DESC')->take(1000)->get();
        }
        if($request->alarma != 'todas' && $request->tipo == 'Prealarma' && $request->datepicker != null ){
            $alarmas = DB::table('alarmas')->where('idalarma',$request->alarma)->wherein('idalarma',[1,11,7,3,13])->whereBetween('startdate',[$request->datepicker,$request->datepicker2])->orderby('id','DESC')->take(1000)->get();
        }
        if($request->alarma != 'todas' && $request->tipo == 'Alarma' && $request->datepicker != null ){
            $alarmas = DB::table('alarmas')->where('idalarma',$request->alarma)->wherein('idalarma',[4,16])->whereBetween('startdate',[$request->datepicker,$request->datepicker2])->orderby('id','DESC')->take(1000)->get();
        }
        return view('admin.historial_incidencias')->with('alarmas',$alarmas)->with('alarmasbd',$alarmasbd)->with('eventos',$eventos)->with('usuarios',$users);
    }
    public function grafica(){
        $alarmasdb = Alarma::all();
        $nombrespre = array();
        $nombresal = array();
        $start = Carbon::now()->subDays(180);
        $nombrespre=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalpre'))->wherein('idalarma',[1,11,7,3,13])->whereDate('startdate',">=",$start )->groupBy('startdate')->orderBy('startdate')->get();                       
        $nombresal=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalalar'))->wherein('idalarma',[4,12])->whereDate('startdate',">=",$start )->groupBy('startdate')->orderBy('startdate')->get();
        return view('grafica')->with('prealarmas',$nombrespre)->with('alarmas',$nombresal)->with('start',$start);
    }
    public function Online(){
        //$elementos = Elemento::all();
        $elementos = DB::table('elementos')->orderby('fecha','desc')->get();
        $db_ext = \DB::connection('comments');
        return view('graficaonline')->with('elementos',$elementos);
    }
    public function accion($id){
        $alarmasbd = Alarma::all();
        $db_ext = \DB::connection('comments');
        $alarmas = DB::table('alarmas')->orderby('id','DESC')->take(10)->get();
        $alarmaid = DB::table('alarmas')->where('id',$id)->first();
        $eventos = DB::table('eventos')->where('alarma_id',$id)->get();
        return view('accion')->with('alarmas',$alarmas)->with('alarmaid',$alarmaid)->with('alarmasbd',$alarmasbd)->with('eventos',$eventos);
     }
     public function grafica_busqueda(request $request)
    {
        $alarmasdb = Alarma::all();
        $db_ext = \DB::connection('comments');
            if($request->tipo == 'todas' && $request->tiempo == 'dia' && $request->datepicker == null){
                $nombrespre = array();
                $nombresal = array();
                $nombrespre=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalpre'))->wherein('idalarma',[1,11,7,3,13])->whereDate('startdate',">=", Carbon::now()->subDays(1))->groupBy('startdate')->orderBy('startdate')->get();                       
                $nombresal=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalalar'))->wherein('idalarma',[4,12])->whereDate('startdate',">=", Carbon::now()->subDays(1))->groupBy('startdate')->orderBy('startdate')->get();
                return view('grafica')->with('prealarmas',$nombrespre)->with('alarmas',$nombresal);
        }
            if($request->tipo == 'todas' && $request->tiempo == 'semana' && $request->datepicker == null){
                $nombrespre = array();
                $nombresal = array();
                $nombrespre=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalpre'))->wherein('idalarma',[1,11,7,3,13])->whereDate('startdate',">=", Carbon::now()->subDays(6))->groupBy('startdate')->orderBy('startdate')->get();                       
                $nombresal=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalalar'))->wherein('idalarma',[4,12])->whereDate('startdate',">=", Carbon::now()->subDays(6))->groupBy('startdate')->orderBy('startdate')->get();
                return view('grafica')->with('prealarmas',$nombrespre)->with('alarmas',$nombresal);
            }
            if($request->tipo == 'todas' && $request->tiempo == 'mes' && $request->datepicker == null){
                $nombrespre = array();
                $nombresal = array();
                $nombrespre=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalpre'))->wherein('idalarma',[1,11,7,3,13])->whereDate('startdate',">=", Carbon::now()->subDays(30))->groupBy('startdate')->orderBy('startdate')->get();                       
                $nombresal=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalalar'))->wherein('idalarma',[4,12])->whereDate('startdate',">=", Carbon::now()->subDays(30))->groupBy('startdate')->orderBy('startdate')->get();
                return view('grafica')->with('prealarmas',$nombrespre)->with('alarmas',$nombresal);
            }
            if($request->tipo != "todas" && $request->tiempo == 'dia' && $request->datepicker == null){
                $nombrespre = array();
                $nombresal = array();
                if($request->tipo == 1 ||$request->tipo == 3 ||$request->tipo == 7 || $request->tipo == 11 || $request->tipo == 13){
                    $nombrespre=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalpre'))->where('idalarma',$request->tipo)->whereDate('startdate',">=", Carbon::now()->subDays(1))->groupBy('startdate')->orderBy('startdate')->get();
                    $nombresal=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalalar'))->where('idalarma',2)->whereDate('startdate',">=", Carbon::now()->subDays(1))->groupBy('startdate')->orderBy('startdate')->get();
                }else{
                    $nombrespre=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalpre'))->where('idalarma',2)->whereDate('startdate',">=", Carbon::now()->subDays(1))->groupBy('startdate')->orderBy('startdate')->get();
                    $nombresal=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalalar'))->where('idalarma',$request->tipo)->whereDate('startdate',">=", Carbon::now()->subDays(1))->groupBy('startdate')->orderBy('startdate')->get();  
                }
                return view('grafica')->with('prealarmas',$nombrespre)->with('alarmas',$nombresal);
            }
            if($request->tipo != "todas" && $request->tiempo == 'semana'  && $request->datepicker == null){
                $nombrespre = array();
                $nombresal = array();
                if($request->tipo == 1 ||$request->tipo == 3 ||$request->tipo == 7 || $request->tipo == 11 || $request->tipo == 13){
                    $nombrespre=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalpre'))->where('idalarma',$request->tipo)->whereDate('startdate',">=", Carbon::now()->subDays(6))->groupBy('startdate')->orderBy('startdate')->get();
                    $nombresal=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalalar'))->where('idalarma',2)->whereDate('startdate',">=", Carbon::now()->subDays(6))->groupBy('startdate')->orderBy('startdate')->get();
                }else{
                    $nombrespre=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalpre'))->where('idalarma',2)->whereDate('startdate',">=", Carbon::now()->subDays(6))->groupBy('startdate')->orderBy('startdate')->get();
                    $nombresal=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalalar'))->where('idalarma',$request->tipo)->whereDate('startdate',">=", Carbon::now()->subDays(6))->groupBy('startdate')->orderBy('startdate')->get(); 
                }
               
                return view('grafica')->with('prealarmas',$nombrespre)->with('alarmas',$nombresal);
            }
            if($request->tipo != "todas" && $request->tiempo == 'mes' && $request->datepicker == null){
                $nombrespre = array();
                $nombresal = array();
                if($request->tipo == 1 ||$request->tipo == 3 ||$request->tipo == 7 || $request->tipo == 11 || $request->tipo == 13){
                    $nombrespre=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalpre'))->where('idalarma',$request->tipo)->whereDate('startdate',">=", Carbon::now()->subDays(30))->groupBy('startdate')->orderBy('startdate')->get();
                    $nombresal=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalalar'))->where('idalarma',2)->whereDate('startdate',">=", Carbon::now()->subDays(30))->groupBy('startdate')->orderBy('startdate')->get();
                }else{
                    $nombrespre=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalpre'))->where('idalarma',2)->whereDate('startdate',">=", Carbon::now()->subDays(30))->groupBy('startdate')->orderBy('startdate')->get();
                    $nombresal=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalalar'))->where('idalarma',$request->tipo)->whereDate('startdate',">=", Carbon::now()->subDays(30))->groupBy('startdate')->orderBy('startdate')->get();  
                }
                return view('grafica')->with('prealarmas',$nombrespre)->with('alarmas',$nombresal);
            }
            if($request->tipo == "todas" && $request->tiempo == 'mes' && $request->datepicker != null){
                $nombrespre = array();
                $nombresal = array();
                $start = Carbon::parse($request->datepicker)->addDays(30);
                    $nombrespre=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalpre'))->wherein('idalarma',[1,11,7,3,13])->whereBetween('startdate',[$request->datepicker, $start])->groupBy('startdate')->orderBy('startdate')->get();
                    $nombresal=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalalar'))->wherein('idalarma',[4,12])->whereBetween('startdate',[$request->datepicker, $start])->groupBy('startdate')->orderBy('startdate')->get();
              
                return view('grafica')->with('prealarmas',$nombrespre)->with('alarmas',$nombresal);
            }
            if($request->tipo == "todas" && $request->tiempo == 'semana' && $request->datepicker != null){
                $nombrespre = array();
                $nombresal = array();
                $start = Carbon::parse($request->datepicker)->addDays(6);
                    $nombrespre=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalpre'))->wherein('idalarma',[1,11,7,3,13])->whereBetween('startdate',[$request->datepicker, $start])->groupBy('startdate')->orderBy('startdate')->get();
                    $nombresal=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalalar'))->wherein('idalarma',[4,12])->whereBetween('startdate',[$request->datepicker, $start])->groupBy('startdate')->orderBy('startdate')->get();
              
                return view('grafica')->with('prealarmas',$nombrespre)->with('alarmas',$nombresal);
            }
            if($request->tipo == "todas" && $request->tiempo == 'dia' && $request->datepicker != null){
                $nombrespre = array();
                $nombresal = array();
                $start = Carbon::parse($request->datepicker)->addDays(1);
                    $nombrespre=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalpre'))->wherein('idalarma',[1,11,7,3,13])->whereBetween('startdate',[$request->datepicker, $start])->groupBy('startdate')->orderBy('startdate')->get();
                    $nombresal=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalalar'))->wherein('idalarma',[4,12])->whereBetween('startdate',[$request->datepicker, $start])->groupBy('startdate')->orderBy('startdate')->get();
              
                return view('grafica')->with('prealarmas',$nombrespre)->with('alarmas',$nombresal);
            }
            if($request->tipo != "todas" && $request->tiempo == 'mes' && $request->datepicker != null){
                $nombrespre = array();
                $nombresal = array();
                $start = Carbon::parse($request->datepicker)->addDays(30);
                if($request->tipo == 1 ||$request->tipo == 3 ||$request->tipo == 7 || $request->tipo == 11 || $request->tipo == 13){
                    $nombrespre=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalpre'))->where('idalarma',$request->tipo)->whereBetween('startdate',[$request->datepicker, $start])->groupBy('startdate')->orderBy('startdate')->get();
                    $nombresal=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalalar'))->where('idalarma',2)->whereBetween('startdate',[$request->datepicker, $start])->groupBy('startdate')->orderBy('startdate')->get();
                }else{
                    $nombrespre=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalpre'))->where('idalarma',2)->whereBetween('startdate',[$request->datepicker, $start])->groupBy('startdate')->orderBy('startdate')->get();
                    $nombresal=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalalar'))->where('idalarma',$request->tipo)->whereBetween('startdate',[$request->datepicker, $start])->groupBy('startdate')->orderBy('startdate')->get();  
                }
                return view('grafica')->with('prealarmas',$nombrespre)->with('alarmas',$nombresal);
            }
            if($request->tipo != "todas" && $request->tiempo == 'semana' && $request->datepicker != null){
                $nombrespre = array();
                $nombresal = array();
                $start = Carbon::parse($request->datepicker)->addDays(6);
                if($request->tipo == 1 ||$request->tipo == 3 ||$request->tipo == 7 || $request->tipo == 11 || $request->tipo == 13){
                    $nombrespre=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalpre'))->where('idalarma',$request->tipo)->whereBetween('startdate',[$request->datepicker, $start])->groupBy('startdate')->orderBy('startdate')->get();
                    $nombresal=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalalar'))->where('idalarma',2)->whereBetween('startdate',[$request->datepicker, $start])->whereNotBetween('startdate',[$start->addDays(8), $start->addDays(60)])->groupBy('startdate')->orderBy('startdate')->get();
                }else{
                    $nombrespre=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalpre'))->where('idalarma',2)->whereBetween('startdate',[$request->datepicker, $start])->groupBy('startdate')->orderBy('startdate')->get();
                    $nombresal=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalalar'))->where('idalarma',$request->tipo)->whereBetween('startdate',[$request->datepicker, $start])->whereNotBetween('startdate',[$start->addDays(8), $start->addDays(60)])->groupBy('startdate')->orderBy('startdate')->get();  
                }
                return view('grafica')->with('prealarmas',$nombrespre)->with('alarmas',$nombresal);
            }
            if($request->tipo != "todas" && $request->tiempo == 'dia' && $request->datepicker != null){
                $nombrespre = array();
                $nombresal = array();
                $start = Carbon::parse($request->datepicker)->addDays(1);
                if($request->tipo == 1 ||$request->tipo == 3 ||$request->tipo == 7 || $request->tipo == 11 || $request->tipo == 13){
                    $nombrespre=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalpre'))->where('idalarma',$request->tipo)->whereBetween('startdate',[$request->datepicker, $start])->groupBy('startdate')->orderBy('startdate')->get();
                    $nombresal=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalalar'))->where('idalarma',2)->whereBetween('startdate',[$request->datepicker, $start])->groupBy('startdate')->orderBy('startdate')->get();
                }else{
                    $nombrespre=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalpre'))->where('idalarma',2)->whereBetween('startdate',[$request->datepicker, $start])->groupBy('startdate')->orderBy('startdate')->get();
                    $nombresal=DB::table('alarmas')->select('startdate',DB::raw('count(*) AS totalalar'))->where('idalarma',$request->tipo)->whereBetween('startdate',[$request->datepicker, $start])->groupBy('startdate')->orderBy('startdate')->get();  
                }
                return view('grafica')->with('prealarmas',$nombrespre)->with('alarmas',$nombresal);
            }
      
    }
    public function historial_incidencia(){
       
        return Excel::download(new AlarmasExport, 'alarmas.xlsx');
    }
}   