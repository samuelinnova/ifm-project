<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MailController@send');
Route::get('/admin', 'HomeController@admin')->name('admin');
Auth::routes();

Route::group(['middleware'=>'auth:web'],function(){
    Route::group(['middleware'=>'admin'],function(){
        Route::get('/admin', 'HomeController@admin')->name('admin');
    
       
    });
    Route::get('/admin/edit/{id}','UserCustomController@index');
    Route::get('/admin/delete/{id}','UserCustomController@destroy');
    Route::get('/admin/create','UserCustomController@create');
    Route::get('/admin/historial_incidencias','Alarmaifm@historialincidencias');
    Route::post('/admin/historial_incidencias/busqueda','Alarmaifm@historialincidencias_busqueda');
    Route::post('/ifm/admin/crear', 'UserCustomController@create_web');
    Route::post('/ifm/admin/update/{id}', 'UserCustomController@update');
    Route::get('/admin/users_list', 'UserCustomController@showAll');
    Route::get('/change_password', 'UserCustomController@show_web');
    Route::post('/cambiar_password/update/{id}', 'UserCustomController@update_web');
    Route::get('/home','Alarmaifm@getTest');
    Route::get('/tabla/{id}','Alarmaifm@mostrarTabla');
    Route::get('/accion/{id}','Alarmaifm@accion');
    Route::post('/busqueda/grafica','Alarmaifm@grafica_busqueda');
    Route::post('/busqueda','Alarmaifm@busqueda');
    Route::get('/grafica','Alarmaifm@grafica');
    Route::get('/online','Alarmaifm@Online');
    Route::post('/registro/{id}','EventoController@registro');
    Route::post('/busqueda/grafica','Alarmaifm@grafica_busqueda');
    Route::get('/export-incidencias', 'Alarmaifm@historial_incidencia');
});

Route::get('admin/routes' ,'HomeController@admin')->middleware('admin');
