@extends('layouts.app')

@section('content')
<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="border-right" style="background-color:#4A82C3; position: fixed;height: 100%;  z-index: 3000;" id="sidebar-wrapper">
      <div class="sidebar-heading" style="padding-left:9%;background-color:white;"><img src="descarga.jpg" alt="" width="130px;" height="32px" style="margin-left:24px"></div>
      <div class="list-group list-group-flush">
      <a  id="incidencia" href="{{url('/home')}}" class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-exclamation-triangle pr-3"></i>Incidencia</a>
      <a  id="analisiso" href="{{url('/online')}}" class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-chart-area pr-3"></i>Análisis Online</a>
      @if(auth()->user()->rol == 1)
        <a  id="analisis"href="{{url('/grafica')}}"class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-chart-bar pr-3"></i>Análisis</a>
        <a  id="historial"href="{{url('/admin/historial_incidencias')}}"class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-clipboard-list pr-3"></i>Historial de Incidencias</a>
        <a  id="usuarios"href="{{url('/admin/users_list')}}"class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-users text-white mr-2"></i>Usuarios</a>
      @endif             
      </div>
      <a class="list-group-item list-group-item-action text-white menu" style="font-size:15px;background-color:#4A82C3;position:absolute;bottom:0px;justify-content:center">InnovaIT V1.0</a>
    </div>
    <!-- /#sidebar-wrapper -->
  <style>
  .menu:hover{
    background-color:#394880!important;
  }
  </style>
    <!-- Page Content -->
    <div id="page-content-wrapper">

        <nav class="navbar navbar-expand-lg navbar-light  border-bottom " style=" background-color: #4A82C3; height: 60.5px; position: fixed;    width: 100%; z-index: 100;">
       

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
          @guest
                            <li class="nav-item">
                                <a class="nav-link text-white" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a id="navbarDropdown" class="nav-link text-white "    aria-haspopup="true" aria-expanded="false" v-pre>
                                    Operario: {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                            </li>
                        @endguest
            <li class="nav-item">
              <a class="nav-link text-white" > {{ date('H:i') }} </a>
            </li>
            <!--<li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Something else here</a>
              </div>
            </li>-->
            <li class="nav-item">
            <a href="{{url('/change_password')}}" class="nav-link"><i class="fas fa-cogs" style="color:white"></i></a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"
              onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
              <i class="fas fa-power-off text-white"></i>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
            </form>
            </li>
          </ul>
        </div>
      </nav>
      </div>
    
<div class="col-lg-12">
  <form action="/busqueda/grafica" method="post">
  @csrf
      <div class="col-lg-12 row" style="padding-left:32%;margin-top: 5%;">
        <div class="col-lg-3 mt-5" style="">
          <h5 class="mt-2">Tipos</h5>
            <select class="w-50 " name="tipo" style="margin-top:4px">
              <option value="todas" id="todas">Todas</option>
              <option value="1" id="Prealarma A Peak S01">Prealarma A Peak S01</option>
              <option value="3" id="Prealarma A Peak S02">Prealarma A Peak S02</option>
              <option value="11" id="prealarmaapeaks03">Prealarma A Peak S03</option>
              <option value="13" id="prealarmaapeaks04">Prealarma A Peak S04</option>
              <option value="7" id="prealarmacavitacion">Prealarma Cavitacion S02</option>
              <option value="4" id="alarmaapeaks02">Alarma A Peak S02</option>
              <option value="12" id="alarmaapeaks03">Alarma A Peak S03</option>
            </select>
        </div>
        <div class="col-lg-3 mt-5">
          <h5 class="mt-2">Tiempo</h5>
            <select class="w-50" name="tiempo" style="margin-top:5px">
              <option value="dia" id="dia">Dia</option>
              <option value="semana" id="semana">Semana</option>
              <option value="mes" id="mes">Mes</option>
            </select>
        </div>
        <div class="col-lg-3 mt-5">
          <h5 class="mt-2">Fecha</h5>
          <input class="w-50 " type="date" name="datepicker" id="datepicker3">
          <button type="submit" class="btn btn-outline-primary" style="margin-left: 15px;"><i class="fas fa-search"></i></button>
        </div>
        
      </div>
      
    </form>
  <div id="grafica" style="min-width: 290px; width:70%;height: 500px; margin-top:2%;margin-left:22%"></div>
</div>
<script type="text/javascript">
Highcharts.chart('grafica', {
  chart: {
         type:'column',
      },
      title: {
        text:'Historial de Alarmas y Prealarmas'
      },
      xAxis: {
         type: 'datetime',
        
         
      },
      credits: {
         enabled: false
      },
series: [{
    name: 'Prealarmas',
    data: [
      <?php
      foreach($prealarmas as $prealarma){
      ?>
      [ Date.UTC(<?php echo date("Y",strtotime($prealarma->startdate));?>,<?php echo date("n - 1",strtotime($prealarma->startdate));?>,<?php echo date("d",strtotime($prealarma->startdate));?>),<?php echo $prealarma->totalpre ?>],
      <?php
      }
      ?>
      
    ]
},{
    name: 'Alarmas',
    data: [
      <?php
      foreach($alarmas as $alarma){
      ?>
      [ Date.UTC(<?php echo date("Y",strtotime($alarma->startdate));?>,<?php echo date("n - 1",strtotime($alarma->startdate));?>,<?php echo date("d",strtotime($alarma->startdate));?>),<?php echo $alarma->totalalar ?>],

      <?php
      }
      ?>
      
    ]
}],

responsive: {
    rules: [{
        condition: {
            maxWidth: 500
        },
        chartOptions: {
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom'
            }
        }
    }]
}

});
</script>
 <style>
     html, body {
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
                overflow-y:hidden;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }


            .content {
                text-align: center;
                background-color:#EEEEEE;
                width:100%;
                height:189%;
                display:flex;
                align-content:center;
                align-items:center;
                justify-content:center;
                flex-direction:column;
            }


            .formulario {
            border: none;
            padding: 16px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            transition-duration: 0.4s;
            cursor: pointer;
            margin-top:20px;
}
            }
            
            .m-b-md {
                margin-bottom: 30px;
            }
	  body {
  overflow-x: hidden;
  
}

#sidebar-wrapper {
  min-height: 100vh;
  margin-left: -15rem;
  -webkit-transition: margin .25s ease-out;
  -moz-transition: margin .25s ease-out;
  -o-transition: margin .25s ease-out;
  transition: margin .25s ease-out;
}

#sidebar-wrapper .sidebar-heading {
  padding: 0.875rem 1.25rem;
  font-size: 1.2rem;
}

#sidebar-wrapper .list-group {
  width: 15rem;
}

#page-content-wrapper {
  min-width: 100vw;
}

#wrapper.toggled #sidebar-wrapper {
  margin-left: 0;
}

@media (min-width: 768px) {
  #sidebar-wrapper {
    margin-left: 0;
  }

  #page-content-wrapper {
    min-width: 0;
    width: 100%;
  }

  #wrapper.toggled #sidebar-wrapper {
    margin-left: -15rem;
  }
}
</style>

  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>
@endsection
