@extends('layouts.app')

@section('content')
<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="border-right" style="background-color:#4A82C3; position: fixed;height: 100%;  z-index: 3000;" id="sidebar-wrapper">
      <div class="sidebar-heading" style="padding-left:12%;background-color:white;"><img src="public/descarga.jpg" alt="" width="170px;" height="32px"></div>
      <div class="list-group list-group-flush border-success">
      <a  id="incidencia" href="{{url('/home')}}" class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-exclamation-triangle pr-3"></i>Incidencia</a>
      <a  id="analisiso" href="{{url('/elemento/{id}')}}" class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-chart-area pr-3"></i>Análisis Online</a>
      @if(auth()->user()->rol == 1)
        <a  id="analisis"href="{{url('/grafica')}}"class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-chart-bar pr-3"></i>Análisis</a>
        <a  id="historial"href="{{url('/admin/historial_incidencias')}}"class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-clipboard-list pr-3"></i>Historial de Incidencias</a>
        <a  id="usuarios"href="{{url('/admin/users_list')}}"class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-users text-white mr-2"></i>Usuarios</a>
      @endif             
      </div>
      <a class="list-group-item list-group-item-action text-white menu" style="font-size:15px;background-color:#4A82C3;position:absolute;bottom:0px;justify-content:center">InnovaIT V1.0</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->
  <style>
  .menu:hover{
    background-color:#394880!important;
  }
  </style>
    <!-- Page Content -->
    <div id="page-content-wrapper">

        <nav class="navbar navbar-expand-lg navbar-light  border-bottom " style=" background-color: #4A82C3; height: 59px; position: fixed;    width: 100%; z-index: 100;">
       

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
          @guest
                            <li class="nav-item">
                                <a class="nav-link text-white" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a id="navbarDropdown" class="nav-link text-white "    aria-haspopup="true" aria-expanded="false" v-pre>
                                    Operario: {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                            </li>
                        @endguest
            <li class="nav-item">
              <a class="nav-link text-white" > {{ date('H:i') }} </a>
            </li>
            <!--<li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Something else here</a>
              </div>
            </li>-->
            <li class="nav-item">
            <a href="{{url('/change_password')}}" class="nav-link"><i class="fas fa-cogs" style="color:white"></i></a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"
              onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
              <i class="fas fa-power-off text-white"></i>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
            </form>
            </li>
          </ul>
        </div>
        </div>
      </nav>
      
      <div class="col-lg-12" style="padding-left:15%;padding-top:5%;">
      <div class="row">
      <div class="col-lg-6" style="padding-left:13%;margin-top:30px;">
      <p style="font-size:40px;margin-top:10px;">Acciones</p>
        <ul>
            <li>Id : {{$alarmaid->id}}</li>
            <li>Fecha : {{$alarmaid->created_at}}</li>
            <li>Descripción : {{$alarmaid->descripcion_alarma}}</li>
            <li>Id Instalación: {{$alarmaid->id_instalacion}}</li>
        </ul>
      </div>
      <div class="col-lg-6" style="">
        <form action="{{ url('/registro/$alarmaid->id') }}" method="post">
        @csrf
        <input type="text" value="{{$alarmaid->id}}" id="id" name="id" style="visibility:hidden">
            <div class="container-fluid" style= "width: 100%;">
                <div class="col-lg-12" style="margin-top: 13px;">
                  <p style="font-size:25px;" >Observaciones</p>
                </div>
                
                <div class="col-lg-12">
                  <textarea class="form-control" style="height:90px;width:60%;margin-left:18px" aria-label="With textarea" name="observacion" required></textarea>
                </div>
                <div class="col-lg-12 row mt-3">
                      <p style="font-size:20px;margin-left:18px;">Acción</p>
                      <select class="ml-3 h-50" style="width:300px"  name="accion">
                        <option value="Revisión en proceso">Revisión en proceso.</option>
                        <option value="Pieza solicitada">Pieza solicitada.</option>
                        <option value="Cerrada">Cerrada.</option>
                      </select>
                </div>
                <div class="col-lg-12 row mt-3" style="display:flex;justify-content:center;margin-left:10px;">
                  <button type="submit" class="btn btn-success" aria-hidden="true" style="display:flex;justify-content:center;" >Aplicar</button>
                  <a href="{{url('/home')}}" class="btn btn-danger"  style="display:flex;justify-content:center;margin-left:6px;">Cerrar</a>
                </div>
        </form>
        </div>
        </div>
        <div class="col-lg-12">
          <p style="font-size:40px;margin-left:12%;" >Registros</p>
        </div>
        <div class="col-lg-12 ml-3">
                <table style="padding-left:-43px">
                  <thead>
                    <tr class="table100-head"> 
                      <th class="column2">Fecha</th>
                      <th class="column2">Id Usuario</th>
                      <th class="column2">Acción</th>
                      <th class="column2">Observaciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($eventos as $evento)
                        <tr>
                          <td class="column2">{{$evento->created_at}}</td>
                          <td class="column2">{{$evento->user_id}}</td>
                          <td class="column2">{{$evento->accion}}</td>
                          <td class="column2 ">{{$evento->observacion}}</td>
                        </tr>
                  @endforeach					
                  </tbody>
              </table>
        </div>
      </div>
 <style>
     html, body {
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
  
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }


            .content {
                text-align: center;
                background-color:#EEEEEE;
                width:100%;
                height:189%;
                display:flex;
                align-content:center;
                align-items:center;
                justify-content:center;
                flex-direction:column;
            }


            .formulario {
            border: none;
            padding: 16px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            transition-duration: 0.4s;
            cursor: pointer;
            margin-top:20px;
}
            }
            
            .m-b-md {
                margin-bottom: 30px;
            }
	  body {
  overflow-x: hidden;
  
}
p {
        font-family: Helvetica, Arial, sans-serif;
        font-weight: lighter;
    }

#sidebar-wrapper {
  min-height: 100vh;
  margin-left: -15rem;
  -webkit-transition: margin .25s ease-out;
  -moz-transition: margin .25s ease-out;
  -o-transition: margin .25s ease-out;
  transition: margin .25s ease-out;
}

#sidebar-wrapper .sidebar-heading {
  padding: 0.875rem 1.25rem;
  font-size: 1.2rem;
}

#sidebar-wrapper .list-group {
  width: 15rem;
}

#page-content-wrapper {
  min-width: 100vw;
}

#wrapper.toggled #sidebar-wrapper {
  margin-left: 0;
}

@media (min-width: 768px) {
  #sidebar-wrapper {
    margin-left: 0;
  }

  #page-content-wrapper {
    min-width: 0;
    width: 100%;
  }

  #wrapper.toggled #sidebar-wrapper {
    margin-left: -15rem;
  }
}
.container-table100 {
  width: 100%;
  min-height: 100vh;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  
}

.wrap-table100 {
  width: 900px;
}

table {
  border-spacing: 1;
  border-collapse: collapse;
  background: white;
  border-radius: 10px;
  overflow: hidden;
  width: 80%;
  margin: 0 auto;
  position: relative;
}
table * {
  position: relative;
}
table td, table th {
  padding-left: 8px;
}
table thead tr {
  height: 60px;
  background: #4A82C3;
}
table tbody tr {
  height: 50px;
  background:#EEEEEE;
}
table tbody tr:last-child {
  border: 0;
}
table td, table th {
  text-align: left;
}
table td.l, table th.l {
  text-align: right;
}
table td.c, table th.c {
  text-align: center;
}
table td.r, table th.r {
  text-align: center;
}


.table100-head th{
  font-size: 18px;
  color: #fff;
  line-height: 1.2;
  font-weight: unset;
}

tbody tr:nth-child(even) {
  background-color: #f5f5f5;
}

tbody tr {
  font-size: 15px;
  color: #808080;
  line-height: 1.2;
  font-weight: unset;
}

tbody tr:hover {
  color: #555555;
  background-color: #f5f5f5;
  cursor: pointer;
}

.column1 {
  text-align:center;
}

.column2 {
  text-align:center;
}

.column3 {
 text-align:center;
}

.column4 {
  text-align: center;
}

.column5 {
  text-align: center;
}
.column6{
  text-align:center;
}

@media screen and (max-width: 992px) {
  table {
    display: block;
  }
  table > *, table tr, table td, table th {
    display: block;
  }
  table thead {
    display: none;
  }
  table tbody tr {
    height: auto;
    padding: 37px 0;
  }
  table tbody tr td {
    margin-bottom: 24px;
  }
  table tbody tr td:last-child {
    margin-bottom: 0;
  }
  table tbody tr td:before {
    font-family: OpenSans-Regular;
    font-size: 14px;
    color: #999999;
    line-height: 1.2;
    font-weight: unset;
    position: absolute;
    width: 40%;
    left: 30px;
    top: 0;
  }
  table tbody tr td:nth-child(1):before {
    content: "Fecha";
  }
  table tbody tr td:nth-child(2):before {
    content: "ID Alarma";
  }
  table tbody tr td:nth-child(3):before {
    content: "Descripcion Alarma";
  }
  table tbody tr td:nth-child(4):before {
    content: "Valor";
  }
  table tbody tr td:nth-child(5):before {
    content: "Error";
  }
  table tbody tr td:nth-child(6):before {
    content: "Acciones";
  }

  .column4,
  .column5,
  .column6 {
    text-align: center;
  }

  .column4,
  .column5,
  .column6,
  .column1,
  .column2,
  .column3 {
    width: 100%;
   
  }

  tbody tr {
    font-size: 14px;
  }
}

@media (max-width: 576px) {
  .container-table100 {
    padding-left: 15px;
    padding-right: 15px;
  }
}
</style>

  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>
@endsection
