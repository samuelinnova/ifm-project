
@extends('layouts.app')

@section('content')

  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
 <div class="border-right" style="background-color:#4A82C3; position: fixed;height: 100%;  z-index: 3000;" id="sidebar-wrapper">
  <div class="sidebar-heading" style="padding-left:9%;background-color:white;"><img src="descarga.jpg" alt="" width="130px;" height="32px" style="margin-left:24px"></div>
  <div class="list-group list-group-flush">
   <a  id="incidencia" href="{{url('/home')}}" class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-exclamation-triangle pr-3"></i>Incidencia</a>
   <a  id="analisiso" href="{{url('/online')}}" class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-chart-area pr-3"></i>Análisis Online</a>
    @if(auth()->user()->rol == 1)
    <a  id="analisis"href="{{url('/grafica')}}"class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-chart-bar pr-3"></i>Análisis</a>
    <a  id="analisis"href="{{url('/admin/historial_incidencias')}}"class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-clipboard-list pr-3"></i>Historial de Incidencias</a>
    <a  id="usuarios"href="{{url('/admin/users_list')}}"class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-users text-white mr-2"></i>Usuarios</a>
    @endif        
  </div>
  <a class="list-group-item list-group-item-action text-white menu" style="font-size:15px;background-color:#4A82C3;position:absolute;bottom:0px;justify-content:center">InnovaIT V1.0</a>   
</div>
<!-- /#sidebar-wrapper -->
  <style>
  .menu:hover{
    background-color:#394880!important;
  }
  </style>
    <!-- Page Content -->
    <div id="page-content-wrapper">

        <nav class="navbar navbar-expand-lg navbar-light  border-bottom border-success" style=" background-color: #4A82C3; height: 62px; position: fixed;    width: 100%; z-index: 100;">
       

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
          @guest
            <li class="nav-item">
                <a class="nav-link text-white" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
          @else
            <li class="nav-item">
                <a id="navbarDropdown" class="nav-link text-white "    aria-haspopup="true" aria-expanded="false" v-pre>
                    Operario: {{ Auth::user()->name }} <span class="caret"></span>
                </a>
            </li>
          @endguest
            <li class="nav-item">
              <a class="nav-link text-white" >Lote actual:  <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-white" > {{ date('H:i') }} </a>
            </li>
            <!--<li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Something else here</a>
              </div>
            </li>-->
            <li class="nav-item">
            <a href="{{url('/change_password')}}" class="nav-link text-white "><i class="fas fa-cogs"  ></i></a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"
              onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
              <i class="fas fa-power-off text-white"></i>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
            </form>
            </li>
          </ul>
        </div>
      </nav>
      <div class="limiter">
		<div  class="container-table100" style= "width: 100%; padding-right: 0px; padding-left: 15%; padding-top: 5%; margin-right: auto; margin-left: auto;"  style= "width: 100%; padding-right: 0px; padding-left: 15%; padding-top: 5%; margin-right: auto; margin-left: auto;">
			<div class="wrap-table100">
				<div class="table100">
        <div class="row"><div class="col-lg-6"><h1 style="color:black;"> Tabla de Usuarios</h1></div><div class="col-lg-6"><a type="submit" href="{{url('/admin/create')}}"  class="boton" style="float:right;margin-bottom:20px">Crear Usuario</a></div> </div>
					<table>
						<thead>
							<tr class="table100-head"> 
								<th class="column1">Fecha de creación</th>
								<th class="column2">ID</th>
								<th class="column3">Nombre</th>
								<th class="column4">Apellido</th>
								<th class="column5">email</th>
								<th class="column6">Acciones</th>
							</tr>
						</thead>
						<tbody>
              @foreach($usuarios as $user)
                  <tr>
                    <td class="column1">{{$user->created_at}}</td>
                    <td class="column2">{{$user->id}}</td>
                    <td class="column3">{{$user->name}}</td>
                    <td class="column4">{{$user->apellido}}</td>
                    <td class="column5">{{$user->email}}</td>
                    <td class="column6"><a type="button" href="/ifm/admin/edit/{{$user->id}}" class="btn" style="background-color:#fd7b00;color:white;">Editar</a><a type="button" href="/ifm/admin/delete/{{$user->id}}" class="btn" style="background-color:red;color:white;margin-inline-end:-23%;">Borrar</a></td>
                  </tr>			
              @endforeach 					
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

  <!-- /#wrapper -->

  <style>
  .boton{
    text-decoration: none;
    font-weight: 200;
    font-size: 13px;
    color: white;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-left: 20px;
    padding-right: 20px;
    background-color:#394880;
    border-color: #d8d8d8;
    border-width: 3px;
    border-style: solid;
    border-radius: 35px;
  }
  .boton:hover{
    color:black;
  }
  html, body {
        color: #636b6f;
        height: 100vh;
        margin: 0;
    }

    .full-height {
        height: 100vh;
    }

    .flex-center {
        align-items: center;
        display: flex;
        justify-content: center;
    }

    .position-ref {
        position: relative;
    }


    .content {
        text-align: center;
        background-color:#EEEEEE;
        width:100%;
        height:91%;
        display:flex;
        align-content:center;
        align-items:center;
        justify-content:center;
        flex-direction:column;
    }


    .formulario {
    border: none;
    padding: 16px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    transition-duration: 0.4s;
    cursor: pointer;
    margin-top:20px;
}
    }
    
    .m-b-md {
        margin-bottom: 30px;
    }
	  body {
  overflow-x: hidden;
}

#sidebar-wrapper {
  min-height: 100vh;
  margin-left: -15rem;
  -webkit-transition: margin .25s ease-out;
  -moz-transition: margin .25s ease-out;
  -o-transition: margin .25s ease-out;
  transition: margin .25s ease-out;
}

#sidebar-wrapper .sidebar-heading {
  padding: 0.875rem 1.25rem;
  font-size: 1.2rem;
}

#sidebar-wrapper .list-group {
  width: 15rem;
}

#page-content-wrapper {
  min-width: 100vw;
}

#wrapper.toggled #sidebar-wrapper {
  margin-left: 0;
}

@media (min-width: 768px) {
  #sidebar-wrapper {
    margin-left: 0;
  }

  #page-content-wrapper {
    min-width: 0;
    width: 100%;
  }

  #wrapper.toggled #sidebar-wrapper {
    margin-left: -15rem;
  }
}
/*//////////////////////////////////////////////////////////////////
[ RESTYLE TAG ]*/
* {
	margin: 0px; 
	padding: 0px; 
	box-sizing: border-box;
}

body, html {
	height: 100%;
	font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";

}

/* ------------------------------------ */
a {
	margin: 0px;
	transition: all 0.4s;
	-webkit-transition: all 0.4s;
  -o-transition: all 0.4s;
  -moz-transition: all 0.4s;
}

a:focus {
	outline: none !important;
}

a:hover {
	text-decoration: none;
}

/* ------------------------------------ */
h1,h2,h3,h4,h5,h6 {margin: 0px;}

p {margin: 0px;}

ul, li {
	margin: 0px;
	list-style-type: none;
}


/* ------------------------------------ */
input {
  display: block;
	outline: none;
	border: none !important;
}

textarea {
  display: block;
  outline: none;
}

textarea:focus, input:focus {
  border-color: transparent !important;
}

/* ------------------------------------ */
button {
	outline: none !important;
	border: none;
	background: transparent;
}

button:hover {
	cursor: pointer;
}

iframe {
	border: none !important;
}
/*//////////////////////////////////////////////////////////////////
[ Utiliti ]*/

/*//////////////////////////////////////////////////////////////////
[ Table ]*/

.limiter {
  width: 100%;
  margin: 0 auto;
}

.container-table100 {
  width: 100%;
  min-height: 100vh;


  display: -webkit-box;
  display: -webkit-flex;
  display: -moz-box;
  display: -ms-flexbox;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  padding: 33px 30px;
}

.wrap-table100 {
  width: 1170px;
}

table {
  border-spacing: 1;
  border-collapse: collapse;
  background: white;
  border-radius: 10px;
  overflow: hidden;
  width: 100%;
  margin: 0 auto;
  position: relative;
}
table * {
  position: relative;
}
table td, table th {
  padding-left: 8px;
}
table thead tr {
  height: 60px;
  background: #4A82C3;
}
table tbody tr {
  height: 50px;
  background:#EEEEEE;
}
table tbody tr:last-child {
  border: 0;
}
table td, table th {
  text-align: left;
}
table td.l, table th.l {
  text-align: right;
}
table td.c, table th.c {
  text-align: center;
}
table td.r, table th.r {
  text-align: center;
}


.table100-head th{
  font-family: OpenSans-Regular;
  font-size: 18px;
  color: #fff;
  line-height: 1.2;
  font-weight: unset;
}

tbody tr:nth-child(even) {
  background-color: #f5f5f5;
}

tbody tr {
  font-family: OpenSans-Regular;
  font-size: 15px;
  color: #808080;
  line-height: 1.2;
  font-weight: unset;
}

tbody tr:hover {
  color: #555555;
  background-color: #f5f5f5;
  cursor: pointer;
}

.column1 {
  width: 260px;
  padding-left: 40px;
}

.column2 {
  width: 160px;
}

.column3 {
  width: 245px;
}

.column4 {
  width: 110px;
  text-align: right;
}

.column5 {
  width: 170px;
  text-align: right;
}

.column6 {
  width: 222px;
  text-align: right;
  padding-right: 62px;
}


@media screen and (max-width: 992px) {
  table {
    display: block;
  }
  table > *, table tr, table td, table th {
    display: block;
  }
  table thead {
    display: none;
  }
  table tbody tr {
    height: auto;
    padding: 37px 0;
  }
  table tbody tr td {
    padding-left: 40% !important;
    margin-bottom: 24px;
  }
  table tbody tr td:last-child {
    margin-bottom: 0;
  }
  table tbody tr td:before {
    font-family: OpenSans-Regular;
    font-size: 14px;
    color: #999999;
    line-height: 1.2;
    font-weight: unset;
    position: absolute;
    width: 40%;
    left: 30px;
    top: 0;
  }
  table tbody tr td:nth-child(1):before {
    content: "Date";
  }
  table tbody tr td:nth-child(2):before {
    content: "Order ID";
  }
  table tbody tr td:nth-child(3):before {
    content: "Name";
  }
  table tbody tr td:nth-child(4):before {
    content: "Price";
  }
  table tbody tr td:nth-child(5):before {
    content: "Quantity";
  }
  table tbody tr td:nth-child(6):before {
    content: "Total";
  }

  .column4,
  .column5,
  .column6 {
    text-align: left;
  }

  .column4,
  .column5,
  .column6,
  .column1,
  .column2,
  .column3 {
    width: 100%;
    

  }

  tbody tr {
    font-size: 14px;
  }
}
@media (max-width: 768px){
  .container-table100{
    padding-left: 32%!important;
    padding-top: 10%!important;
  }
}
@media (max-width: 576px) {
  .container-table100 {
    padding-left: 15px;
    padding-right: 15px;
  }
}
</style>

  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>
@stop
