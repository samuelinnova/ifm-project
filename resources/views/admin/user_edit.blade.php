
 
@extends('layouts.app')

@section('content')

  <div class="d-flex" id="wrapper">

   <!-- Sidebar -->
   <div class="border-right" style="background-color:#4A82C3; position: fixed;height: 100%;  z-index: 3000;" id="sidebar-wrapper">
  <div class="sidebar-heading" style="padding-left:9%;background-color:white;"><img src="descarga.jpg" alt="" width="130px;" height="32px" style="margin-left:24px"></div>
  <div class="list-group list-group-flush">
   <a  id="incidencia" href="{{url('/home')}}" class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-exclamation-triangle pr-3"></i>Incidencia</a>
   <a  id="analisiso" href="{{url('/online')}}" class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-chart-area pr-3"></i>Análisis Online</a>
    @if(auth()->user()->rol == 1)
    <a  id="analisis"href="{{url('/grafica')}}"class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-chart-bar pr-3"></i>Análisis</a>
    <a  id="analisis"href="{{url('/admin/historial_incidencias')}}"class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-clipboard-list pr-3"></i>Historial de Incidencias</a>
    <a  id="usuarios"href="{{url('/admin/users_list')}}"class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-users text-white mr-2"></i>Usuarios</a>
    @endif        
  </div>
  <a class="list-group-item list-group-item-action text-white menu" style="font-size:15px;background-color:#4A82C3;position:absolute;bottom:0px;justify-content:center">InnovaIT V1.0</a>   
</div>
<!-- /#sidebar-wrapper -->
  <style>
  .menu:hover{
    background-color:#394880!important;
  }
  </style>
    <!-- Page Content -->
    <div id="page-content-wrapper">

        <nav class="navbar navbar-expand-lg navbar-light  border-bottom border-success" style=" background-color: #4A82C3; height: 62px; position: fixed;    width: 100%; z-index: 100;">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
          @guest
                            <li class="nav-item">
                                <a class="nav-link text-white" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a id="navbarDropdown" class="nav-link text-white "    aria-haspopup="true" aria-expanded="false" v-pre>
                                    Operario: {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                            </li>
                        @endguest
            <li class="nav-item">
              <a class="nav-link text-white" >Lote actual:  <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-white" > {{ date('H:i') }} </a>
            </li>
            <!--<li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Something else here</a>
              </div>
            </li>-->
            <li class="nav-item">
            <a href="{{url('/change_password')}}" class="nav-link"><i class="fas fa-cogs" style="color:white"></i></a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"
              onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
              <i class="fas fa-power-off text-white"></i>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
            </form>
            </li>
          </ul>
        </div>
      </nav>
      <div class="form-style-2">
<div class="form-style-2-heading" style="    width: 59%;">Actualizar Usuario</div>
<form action="{{ url('/ifm/admin/update/$user->id') }}" method="post">
@csrf
<input type="text" name="id" id="id" value="{{$user->id}}"  style="visibility:hidden">
<label for="name"><span>Name <span class="required">*</span></span><input type="text" class="input-field" id="name" name="name" value="{{$user->name}}" /></label>
<label for="apellido"><span>Apellido <span class="required">*</span></span><input type="text" class="input-field" id="apellido" name="apellido" value="{{$user->apellido}}" /></label>
<label for="email"><span>Email<span class="required">*</span></span><input type="email" class="input-field" id="email" name="email" value="{{$user->email}}" /></label>
<label for="password"><span>Password <span class="required">*</span></span><input type="password" class="input-field" id="password" name="password"  /></label>
<label for="grupo"><span>grupo <span class="required">*</span></span><input type="text" class="input-field" id="grupo" name="grupo" value="{{$user->grupo_de_trabajo}}" /></label>
<label><span> </span><input type="submit" value="Submit" /></label>
</form>
</div>
  <!-- /#wrapper -->
<script>
  $(document).ready(function(){
    $("#password").change(function() {
        $pass=$('#password').val();
        $r_pass=$('#repetir_password').val();
        if($pass == $r_pass){
        $("#btn").attr("disabled",false);
        }else{
            $("#btn").attr("disabled",true);
        }
    });
    $("#repetir_password").change(function() {
      $pass=$('#password').val();
      $r_pass=$('#repetir_password').val();
      if($pass == $r_pass){
      $("#btn").attr("disabled",false);
      }else{
          $("#btn").attr("disabled",true);
      }
    });
  });
</script>
  <style>
     html, body {
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }


            .content {
                text-align: center;
                background-color:#EEEEEE;
                width:100%;
                height:91%;
                display:flex;
                align-content:center;
                align-items:center;
                justify-content:center;
                flex-direction:column;
            }


            .formulario {
            border: none;
            padding: 16px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            transition-duration: 0.4s;
            cursor: pointer;
            margin-top:20px;
            }
            
            
            .m-b-md {
                margin-bottom: 30px;
            }
	  body {
  overflow-x: hidden;
}

#sidebar-wrapper {
  min-height: 100vh;
  margin-left: -15rem;
  -webkit-transition: margin .25s ease-out;
  -moz-transition: margin .25s ease-out;
  -o-transition: margin .25s ease-out;
  transition: margin .25s ease-out;
}

#sidebar-wrapper .sidebar-heading {
  padding: 0.875rem 1.25rem;
  font-size: 1.2rem;
}

#sidebar-wrapper .list-group {
  width: 15rem;
}

#page-content-wrapper {
  min-width: 100vw;
}

#wrapper.toggled #sidebar-wrapper {
  margin-left: 0;
}

@media (min-width: 768px) {
  #sidebar-wrapper {
    margin-left: 0;
  }

  #page-content-wrapper {
    min-width: 0;
    width: 100%;
  }

  #wrapper.toggled #sidebar-wrapper {
    margin-left: -15rem;
  }
}

</style>
<style>
.form-style-2{
    margin-left:24%!important;
	max-width: 100%;
	padding: 20px 12px 10px 20px;
	font: 13px Arial, Helvetica, sans-serif;
}
.form-style-2-heading{
	font-weight: bold;
	font-style: italic;
	border-bottom: 2px solid #ddd;
	margin-bottom: 20px;
	font-size: 26px;
	padding-bottom: 3px;
}
.form-style-2 label{
	display: block;
	margin: 0px 0px 15px 0px;
}
.form-style-2 label > span{
	width: 100px;
	font-weight: bold;
	float: left;
	padding-top: 16px;
	padding-right: 5px;
}
.form-style-2 span.required{
	color:red;
}
.form-style-2 .tel-number-field{
	width: 40px;
	text-align: center;
}
.form-style-2 input.input-field, .form-style-2 .select-field{
	width: 48%;	
    height:50px;
}
.form-style-2 input.input-field, 
.form-style-2 .tel-number-field, 
.form-style-2 .textarea-field, 
 .form-style-2 .select-field{
	box-sizing: border-box;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	border: 1px solid #C2C2C2;
	box-shadow: 1px 1px 4px #EBEBEB;
	-moz-box-shadow: 1px 1px 4px #EBEBEB;
	-webkit-box-shadow: 1px 1px 4px #EBEBEB;
	border-radius: 3px;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	padding: 7px;
	outline: none;
}
.form-style-2 .input-field:focus, 
.form-style-2 .tel-number-field:focus, 
.form-style-2 .textarea-field:focus,  
.form-style-2 .select-field:focus{
	border: 1px solid #4A82C3;
}
.form-style-2 .textarea-field{
	height:100px;
	width: 55%;
}
.form-style-2 input[type=submit],
.form-style-2 input[type=button]{
    width: 200px;
    height: 50px;
	border: none;
	padding: 8px 15px 8px 15px;
	background:#4A82C3;
	color: #fff;
	box-shadow: 1px 1px 4px #DADADA;
	-moz-box-shadow: 1px 1px 4px #DADADA;
	-webkit-box-shadow: 1px 1px 4px #DADADA;
	border-radius: 3px;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
}
.form-style-2 input[type=submit]:hover,
.form-style-2 input[type=button]:hover{
	background: #394880;
	color: #fff;
}
</style>

  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>
@stop
