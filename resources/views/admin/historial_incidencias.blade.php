@extends('layouts.app')

@section('content')
<div class="d-flex" id="wrapper">

<!-- Sidebar -->
 <div class="border-right" style="background-color:#4A82C3; position: fixed;height: 100%;  z-index: 2;" id="sidebar-wrapper">
  <div class="sidebar-heading" style="padding-left:9%;background-color:white;"><img src="public/descarga.jpg" alt="" width="130px;" height="32px" style="margin-left:24px"></div>
  <div class="list-group list-group-flush">
   <a  id="incidencia" href="{{url('/home')}}" class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-exclamation-triangle pr-3"></i>Incidencia</a>
   <a  id="analisiso" href="{{url('/online')}}" class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-chart-area pr-3"></i>Análisis Online</a>
    @if(auth()->user()->rol == 1)
    <a  id="analisis" href="{{url('/grafica')}}"class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-chart-bar pr-3"></i>Análisis</a>
    <a  id="analisis" href="{{url('/admin/historial_incidencias')}}"class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-clipboard-list pr-3"></i>Historial de Incidencias</a>
    <a  id="usuarios" href="{{url('/admin/users_list')}}"class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-users text-white mr-2"></i>Usuarios</a>
    @endif        
  </div>
  <a class="list-group-item list-group-item-action text-white menu" style="font-size:15px;background-color:#4A82C3;position:absolute;bottom:0px;justify-content:center">InnovaIT V1.0</a>   
</div>
<!-- /#sidebar-wrapper -->
<style>
.menu:hover{
background-color:#394880!important;
}
</style>
<script type="text/javascript">
var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name) {
    if (!table.nodeType) table = document.getElementById(table)
    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
    window.location.href = uri + base64(format(template, ctx))
  }
})()
</script>
<!-- Page Content -->
<div id="page-content-wrapper">
    <nav class="navbar navbar-expand-lg navbar-light  border-bottom" style=" background-color:#4A82C3 ; height: 60.5px; position: fixed;    width: 100%; z-index: 1;">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
      @guest
        <li class="nav-item">
          <a class="nav-link text-white" href="{{ route('login') }}">{{ __('Login') }}</a>
        </li>
        @else
        <li class="nav-item">
          <a id="navbarDropdown" class="nav-link text-white" aria-haspopup="true" aria-expanded="false" v-pre>
            Usuario: {{ Auth::user()->name }} <span class="caret"></span>
          </a>
        </li>
      @endguest
   
        <li class="nav-item">
          <a class="nav-link text-white" > {{ date('H:i') }} </a>
        </li>
        <!--<li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle"  id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Dropdown
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" >Action</a>
            <a class="dropdown-item" >Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" >Something else here</a>
          </div>
        </li>-->
        <li class="nav-item">
        <?php
          $id=auth()->user()->id;
        ?>
        <a href="{{url('/change_password')}}" class="nav-link"><i class="fas fa-cogs text-white"></i></a>
        </li>
        
        <li class="nav-item">
        <a class="nav-link" href="{{ route('logout') }}"
          onclick="event.preventDefault();
          document.getElementById('logout-form').submit();">
          <i class="fas fa-power-off text-white"></i>
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
        </form>
        </li>
      </ul>
    </div>
  </nav>
  <h1 style="margin-top:5%;display:flex;justify-content:center;    margin-left: 9%;">Historial de Incidencias</h1>
  <form action="{{ url('/admin/historial_incidencias/busqueda') }}" method="post">
  @csrf
  <div class="container-fluid" style= "width: 100%; padding-right: 0px; padding-left: 14%;   margin-left: auto;display:flex; justify-content:center;">
    <div class="col-lg-12 row" style="display:flex; justify-content:center;">
      <div class="col-lg-4 row mt-4" style="">
          <h5 class="mt-5" style="">Alarma</h5>
          <select class="w-75  mt-5" name="alarma" style="margin-left:7px;">
          <option value="todas">Todas</option>
              <option value="1" id="Prealarma A Peak S01">Prealarma A Peak S01</option>
              <option value="3" id="Prealarma A Peak S02">Prealarma A Peak S02</option>
              <option value="11" id="prealarmaapeaks03">Prealarma A Peak S03</option>
              <option value="13" id="prealarmaapeaks04">Prealarma A Peak S04</option>
              <option value="7" id="prealarmacavitacion">Prealarma Cavitacion S02</option>
              <option value="4" id="alarmaapeaks02">Alarma A Peak S02</option>
              <option value="12" id="alarmaapeaks03">Alarma A Peak S03</option>
          </select>
      </div>
      <div class="col-lg-4 row mt-4" style="">
          <h5 class="mt-5">Tipos</h5>
          <select class="w-75  mt-5" name="tipo" style="margin-left:7px">
          <option value="todas" id="todas">Todas</option>
            <option value="Prealarma" id="Prealarma">Prealarma</option>
            <option value="Alarma" id="Alarma">Alarma</option>
          </select>
      </div>
      </div>
      </div>
      <div class="col-lg-12 row" style="margin-left:6%; display:flex; justify-content:center;">
        <h5 class="mt-4">Fecha</h5>
        <input class="ml-3 w-25 h-50 mt-4"  type="date" name="datepicker" id="datepicker">
        <h5 class="mt-4 ml-3">a</h5>
        <input class="ml-3 w-25 h-50 mt-4"  type="date" name="datepicker2" id="datepicker2">
        <button type="submit" class="btn btn-outline-primary mt-3 ml-3"><i class="fas fa-search"></i></button>
      </div>
      
      </form>
      <?php $a = 0?>  
      <!--<input type="button"  onclick="exportTableToExcel()" style="background-color:#4A82C3;border:1px solid #4A82C3;color:white;margin-left: 86%;margin-bottom: -2%;" value="Exportar a Excel"></button>-->
      <a  name="btnExportar" class="" id="btnExportar" style=""value="" onclick="ExportToExcel(jQuery('#tabla').prop('outerHTML'))"><img src="/excel.png" style="    width: 1.8%; margin-left: 87%; margin-top: -3.3%;"alt="" srcset=""> </a>
    <div class="col-lg-12 mt-5" style="margin-bottom: 3%!important">
    @if(auth()->user()->rol == 0)
    <table id="tabla" rules="groups" frame="hsides">
						<thead> 
							<tr class="table100-head"> 
                <th class="column1">Fecha</th>
								<th class="column2">Id Alarma</th>
                <th class="column2">Id Equipo</th>
								<th class="column3">Descripcion Alarma</th>
								<th class="column4">Valor</th>
								<th class="column5">Error</th>
								<th class="column6">Acciones</th>
							</tr>
						</thead>
						<tbody>
            <?php  $a=0?>
            @foreach($alarmas as $alarma)
                  @if($alarma->estado_alarma != 3)
                  <tr style="display:hidden">
                    <td class="column1">{{$alarma->startdate}}</td>
                    <td class="column2">{{$alarma->idalarma}}</td>
                    <td class="column2">{{$alarma->id_instalacion}}</td>
                    <td class="column3 ">{{$alarma->descripcion_alarma}}</td>
                    <td class="column4">{{$alarma->valor}}</td>
                    <td class="column5">{{$alarma->error}}</td>
                  
                    @if($alarma->estado_alarma == 1 )
                    <td class="column6 pl-1"><a type="button" class="btn"  href="/tabla/{{$alarma->id}}" style="background-color:#4A82C3;border:1px solid #4A82C3;color:white;">+ Info</a><a type="button" class="btn ml-4"  href="/accion/{{$alarma->id}}" style="background-color:#ea0000;border:1px solid #ea0000;color:white;">Acción</a></td>
                    @endif
                    @if($alarma->estado_alarma == 2 )
                    <td class="column6 pl-1"><a type="button" class="btn"  href="/tabla/{{$alarma->id}}" style="background-color:#4A82C3;border:1px solid #4A82C3;color:white;">+ Info</a><a type="button" class="btn ml-4"  href="/accion/{{$alarma->id}}" style="background-color:#FFA111;border:1px solid #FFA111;color:white;">Acción</a></td>
                    @endif
                  </tr>	
                @endif
              @endforeach					
						</tbody>
		</table>
    @else
    <table id="tabla"style="margin-left:20%!important;">
						<thead>
							<tr class="table100-head"> 
                <th class="column1">Fecha</th>
								<th class="column2">Id Alarma</th>
                <th class="column2">Id Equipo</th>
								<th class="column3">Descripcion Alarma</th>
								<th class="column4">Valor</th>
								<th class="column5">Error</th>
								<th class="column6">Acciones</th>
							</tr>
						</thead>
						<tbody>
            <?php  $a=0?>
            @foreach($alarmas as $alarma)
              <tr>
                <td class="column1">{{$alarma->created_at}}</td>
                <td class="column2">{{$alarma->idalarma}}</td>
                <td class="column2">{{$alarma->id_instalacion}}</td>
                <td class="column3 ">{{$alarma->descripcion_alarma}}</td>
                <td class="column4">{{$alarma->valor}}</td>
                <td class="column5">{{$alarma->error}}</td>
                 
                    @if($alarma->estado_alarma == 1 )
                    <td class="column6 pl-1"><a type="button" class="btn" data-toggle="modal" data-target="#modalinfo-{{$alarma->id}}"  name="indicador" style="background-color:#4A82C3;border:1px solid #4A82C3;color:white;">+ Info</a></td>
                    @endif
                    @if($alarma->estado_alarma == 2 )
                    <td class="column6 pl-1"><a type="button" class="btn" data-toggle="modal" data-target="#modalinfo-{{$alarma->id}}"  name="indicador" style="background-color:#4A82C3;border:1px solid #4A82C3;color:white;">+ Info</a></td>
                    @endif
                    @if($alarma->estado_alarma == 3 )
                    <td class="column6 pl-1"><a type="button" class="btn" data-toggle="modal" data-target="#modalinfo-{{$alarma->id}}"  name="indicador" style="background-color:#4A82C3;border:1px solid #4A82C3;color:white;">+ Info</a></td>
                    @endif
              </tr>	
            @endforeach					
			</tbody>
		</table>
    @endif
    </div>
    @foreach($alarmas as $alarmaid)
<div class="modal fade" id="modalinfo-{{$alarmaid->id}}" tabindex="-1" role="dialog" style="margin-top:4%">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="width: 1076px; margin-left: -240px; ">
      <div class="modal-header text-white" style="background-color:#4A82C3">
        <h5 class="modal-title">Eventos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    </div>
      <div class="modal-body" style="width: 1076px; margin-left: -240px; background-color: white;">
        <div class="col-lg-12 row">
            <div class="col-lg-6" style="width:100%">
                <p style="padding-top:2%">Fecha de la incidencia: {{$alarmaid->startdate}}</p>
                
                @if($alarmaid->estado_alarma == 3)
                <p>Estado de la incidencia: Finalizada</p>
                <p>Fecha de finalización: {{$alarmaid->fecha_cerrada}}</p>
                @elseif($alarmaid->estado_alarma == 3)
                <p>Estado de la incidencia: En proceso</p>
                @else
                <p>Estado de la incidencia: Pendiente a revisión</p>
                @endif
              
            </div>
        </div>
        <?php $a=0 ?>
        @foreach($eventos as $evento)
          @if($evento->alarma_id == $alarmaid->id)
          @if($a == 0)
            <div class="col-lg-12" style="display:flex;justify-content:center; margin-bottom:2%;margin-top:2%">
              <h3>Comentarios</h3>
            </div>
        
          <table style="width:100%">
						<thead>
							<tr class="table100-head"> 
                <th class="column1">Fecha</th>
								<th class="column2">Usuario</th>
                <th class="column2">Comentario</th>
							</tr>
						</thead>
            @endif
          <?php $a++;?>
          @endif
          @endforeach
						<tbody>
            @foreach($eventos as $evento)
          @if($evento->alarma_id == $alarmaid->id)
              <tr>
                <td class="column1">{{$evento->created_at}}</td>
                <td class="column2">   
                @foreach($usuarios as $user)
                  @if($user->id == $evento->user_id)
                    {{$user->email}}
                  @endif
                @endforeach</td>
                <td class="column2">{{$evento->observacion}}</td>
              </tr>	
              @endif	
              @endforeach			
			</tbody>
		</table>
      </div>
  </div>
</div>
@endforeach


<!--Table-->
    </div>
  </div>
@endsection
 <!-- /#wrapper -->
 <style>
	  body {
      overflow-x: hidden;
    }
    p {
        font-family: Helvetica, Arial, sans-serif;
        font-weight: lighter;
    }
    input[type=text]{
      border-bottom: 1px solid #B1B1B1;
    }

    button {
        font-family: Helvetica, Arial, sans-serif;
        font-size: 1.0em;    
    }

    div#progressBar {
        width: 600px; 
        height: 90px;
    }


    #sidebar-wrapper {
      min-height: 100vh;
      margin-left: -15rem;
      -webkit-transition: margin .25s ease-out;
      -moz-transition: margin .25s ease-out;
      -o-transition: margin .25s ease-out;
      transition: margin .25s ease-out;
    }

    #sidebar-wrapper .sidebar-heading {
      padding: 0.875rem 1.25rem;
      font-size: 1.2rem;
    }

    #sidebar-wrapper .list-group {
      width: 15rem;
    }

    #page-content-wrapper {
      min-width: 100vw;
    }

    #wrapper.toggled #sidebar-wrapper {
      margin-left: 0;
    }

    @media (min-width: 768px) {
      #sidebar-wrapper {
        margin-left: 0;
      }

      #page-content-wrapper {
        min-width: 0;
        width: 100%;
      }

      #wrapper.toggled #sidebar-wrapper {
        margin-left: -15rem;
      }
    }
    .limiter {
  width: 100%;
  margin: 0 auto;
}

.container-table100 {
  width: 100%;
  min-height: 100vh;


  display: -webkit-box;
  display: -webkit-flex;
  display: -moz-box;
  display: -ms-flexbox;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  
}

.wrap-table100 {
  width: 1170px;
}

table {
  border-spacing: 1;
  border-collapse: collapse;
  background: white;
  border-radius: 10px;
  overflow: hidden;
  width: 76%;
  margin: 0 auto;
  position: relative;
}
table * {
  position: relative;
}
table td, table th {
  padding-left: 8px;
}
table thead tr {
  height: 60px;
  background: #4A82C3;
}
table tbody tr {
  height: 50px;
  background:#EEEEEE;
}
table tbody tr:last-child {
  border: 0;
}
table td, table th {
  text-align: left;
}
table td.l, table th.l {
  text-align: right;
}
table td.c, table th.c {
  text-align: center;
}
table td.r, table th.r {
  text-align: center;
}


.table100-head th{
  font-size: 18px;
  color: #fff;
  line-height: 1.2;
  font-weight: unset;
}

tbody tr:nth-child(even) {
  background-color: #f5f5f5;
}

tbody tr {
  font-size: 15px;
  color: #808080;
  line-height: 1.2;
  font-weight: unset;
}

tbody tr:hover {
  color: #555555;
  background-color: #f5f5f5;
  cursor: pointer;
}

.column1 {
  text-align:center;
}

.column2 {
  text-align:center;
}

.column3 {
 text-align:center;
}

.column4 {
  text-align: center;
}

.column5 {
  text-align: center;
}
.column6{
  text-align:center;
}

@media screen and (max-width: 992px) {
  table {
    display: block;
  }
  table > *, table tr, table td, table th {
    display: block;
  }
  table thead {
    display: none;
  }
  table tbody tr {
    height: auto;
    padding: 37px 0;
  }
  table tbody tr td {
    margin-bottom: 24px;
  }
  table tbody tr td:last-child {
    margin-bottom: 0;
  }
  table tbody tr td:before {
    font-family: OpenSans-Regular;
    font-size: 14px;
    color: #999999;
    line-height: 1.2;
    font-weight: unset;
    position: absolute;
    width: 40%;
    left: 30px;
    top: 0;
  }
  table tbody tr td:nth-child(1):before {
    content: "Fecha";
  }
  table tbody tr td:nth-child(2):before {
    content: "ID Alarma";
  }
  table tbody tr td:nth-child(3):before {
    content: "Descripcion Alarma";
  }
  table tbody tr td:nth-child(4):before {
    content: "Valor";
  }
  table tbody tr td:nth-child(5):before {
    content: "Error";
  }
  table tbody tr td:nth-child(6):before {
    content: "Acciones";
  }

  .column4,
  .column5,
  .column6 {
    text-align: center;
  }

  .column4,
  .column5,
  .column6,
  .column1,
  .column2,
  .column3 {
    width: 100%;
   
  }

  tbody tr {
    font-size: 14px;
  }
}

@media (max-width: 576px) {
  .container-table100 {
    padding-left: 15px;
    padding-right: 15px;
  }
}
  </style>

    <!-- Menu Toggle Script -->
    <script>
      $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
      });
      $(function(){
    $("#datepicker").datepicker({
        changeMonth: true,
        changeYear: true
    });
});

</script>
<script>
function ExportToExcel(htmlExport) {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    
    //other browser not tested on IE 11
    // If Internet Explorer
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))
    {
        jQuery('body').append(" <iframe id=\"iframeExport\" style=\"display:none\"></iframe>");
        iframeExport.document.open("txt/html", "replace");
        iframeExport.document.write(htmlExport);
        iframeExport.document.close();
        iframeExport.focus();
        var d = new Date();
        var strDate = d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
        sa = iframeExport.document.execCommand("SaveAs", true, "Historial Incidencias "+strDate+".xls");
    }
    else {      
        var link = document.createElement('a');
        var d = new Date();
        var strDate = d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
        document.body.appendChild(link); // Firefox requires the link to be in the body
        link.download = "Historial Incidencias "+strDate+".xls";
        link.href = 'data:application/vnd.ms-excel,' + escape(htmlExport);
        link.click();
        document.body.removeChild(link);
    }
}
</script>
