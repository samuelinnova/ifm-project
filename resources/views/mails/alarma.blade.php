<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Nueva Alarma</title>
</head>
<body>
    <p>Hola, se ha encontrado una nueva alarma en el sistema, aquí están sus características principales:</p>
    <ul>
        <li>Su fecha : {{ $variable->created_at }}</li>
        <li>Su descripción : {{ $variable->descripcion_alarma }}</li>
        <li>Su valor : {{ $variable->valor }}</li>
        <li>Su estado : {{ $variable->estado }}</li>
    </ul>
    <a href="http://3.8.232.87/ifm/login" target="_blank" rel="noopener noreferrer">Haz clic aquí para ir.</a>
</body>
</html>
