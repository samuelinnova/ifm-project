@extends('layouts.app')

@section('content')
<?php 
//require_once("../datos.php");
?>
<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="border-right" style="background-color:#4A82C3; position: fixed;height: 100%;  z-index: 3000;" id="sidebar-wrapper">
      <div class="sidebar-heading" style="padding-left:9%;background-color:white;"><img src="descarga.jpg" alt="" width="130px;" height="32px" style="margin-left:24px"></div>
      <div class="list-group list-group-flush">
      <a  id="incidencia" href="{{url('/home')}}" class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-exclamation-triangle pr-3"></i>Incidencia</a>
      <a  id="analisiso" href="{{url('/online')}}" class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-chart-area pr-3"></i>Análisis Online</a>
      @if(auth()->user()->rol == 1)
        <a  id="analisis"href="{{url('/grafica')}}"class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-chart-bar pr-3"></i>Análisis</a>
        <a  id="historial"href="{{url('/admin/historial_incidencias')}}"class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-clipboard-list pr-3"></i>Historial de Incidencias</a>
        <a  id="usuarios"href="{{url('/admin/users_list')}}"class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-users text-white mr-2"></i>Usuarios</a>
      @endif             
      </div>
      <a class="list-group-item list-group-item-action text-white menu" style="font-size:15px;background-color:#4A82C3;position:absolute;bottom:0px;justify-content:center">InnovaIT V1.0</a>
    </div>
    <!-- /#sidebar-wrapper -->
  <style>
  .menu:hover{
    background-color:#394880!important;
  }
  </style>
    <!-- Page Content -->
    <div id="page-content-wrapper">

        <nav class="navbar navbar-expand-lg navbar-light  border-bottom " style=" background-color: #4A82C3; height: 60.5px; position: fixed;    width: 100%; z-index: 100;">
       

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
          @guest
                            <li class="nav-item">
                                <a class="nav-link text-white" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a id="navbarDropdown" class="nav-link text-white "    aria-haspopup="true" aria-expanded="false" v-pre>
                                    Operario: {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                            </li>
                        @endguest
            <li class="nav-item">
              <a class="nav-link text-white" > {{ date('H:i') }} </a>
            </li>
            <!--<li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Something else here</a>
              </div>
            </li>-->
            <li class="nav-item">
            <a href="{{url('/change_password')}}" class="nav-link"><i class="fas fa-cogs" style="color:white"></i></a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"
              onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
              <i class="fas fa-power-off text-white"></i>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
            </form>
            </li>
          </ul>
        </div>
      </nav>
      </div>

<div id="graficaon" style="min-width: 290px; width:70%;height: 400px; margin-top:14%;margin-left:-79%"></div>

<script type="text/javascript">
    Highcharts.chart('graficaon', {
    chart: {
        type: 'column',
        events: {
           
        }
    },
    title: {
        text: 'Análisis Online del elemento'
    },
    xAxis: {
        categories: [
            'se01_v_rms_valor',
            'se02_v_rms_valor',
            'se03_v_rms_valor',
            'se04_v_rms_valor',
            'se01_a_rms_valor',
            'se02_a_rms_valor',
            'se03_a_rms_valor',
            'se04_a_rms_valor',
            'se01_a_peak_valor',
            'se02_a_peak_valor',
            'se03_a_peak_valor',
            'se04_a_peak_valor',
            'se02_des_valor',
            'se04_des_valor',
            'se04_la_valor',
            'se03_loa_valor',
            'se02_cavit_valor',
            'se02_desalineamiento_valor',
            'se04_desalineamiento_valor',
        ]
      
    },
    yAxis: [{
        min: 0,
        title: {
            text: 'Valor de cada Elemento'
        }
      
    }],
    plotBands: [{
            from: 0,
            to: 333,
            color: 'green'
        }, {
            from: 334,
            to: 666,
            color: 'yellow'
        }, {
            from: 667,
            to: 1000,
            color: 'red'
        }],
    legend: {
        shadow: false
    },
    tooltip: {
        shared: true
    },
    plotOptions: {
        column: {
            grouping: false,
            shadow: false,
            borderWidth: 0
        }
    },
    series: [{
      <?php $valor = 1000  ?>
        name: 'Employees',
        data: [
        
             <?php
             for($i=0;$i<=18;$i++){
             ?>
             [<?php echo $valor ?>],
       
             <?php
             }
             ?>
         
        ],
        pointPadding: 0.3
    }, {
        name: 'Employees Optimized',
        color: 'rgba(126,86,134,.9)',
        data: [<?php echo $elementos[0]->se01_v_rms_valor?>,<?php echo $elementos[0]->se02_v_rms_valor?>,<?php echo $elementos[0]->se03_v_rms_valor?>,<?php echo $elementos[0]->se04_v_rms_valor?>,<?php echo $elementos[0]->se01_a_rms_valor?>,<?php echo $elementos[0]->se02_a_rms_valor?>,<?php echo $elementos[0]->se03_a_rms_valor?>,<?php echo $elementos[0]->se04_a_rms_valor?>,<?php echo $elementos[0]->se01_a_peak_valor?>,<?php echo $elementos[0]->se02_a_peak_valor?>,<?php echo $elementos[0]->se03_a_peak_valor?>,<?php echo $elementos[0]->se04_a_peak_valor?>,<?php echo $elementos[0]->se02_des_valor?>,<?php echo $elementos[0]->se04_des_valor?>,<?php echo $elementos[0]->se04_la_valor?>,<?php echo $elementos[0]->se03_loa_valor?>,<?php echo $elementos[0]->se02_cavit_valor?>,<?php echo $elementos[0]->se02_desalineamiento_valor?>,<?php echo $elementos[0]->se04_desalineamiento_valor?>],
        pointPadding: 0.4
    }]
});
</script>
 <style>
     html, body {
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
                overflow-y:hidden;
            }

            .full-height {
                height: 100vh;
            }
            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }
            .position-ref {
                position: relative;
            }
            .content {
                text-align: center;
                background-color:#EEEEEE;
                width:100%;
                height:189%;
                display:flex;
                align-content:center;
                align-items:center;
                justify-content:center;
                flex-direction:column;
            }


            .formulario {
            border: none;
            padding: 16px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            transition-duration: 0.4s;
            cursor: pointer;
            margin-top:20px;
}
            }
            
            .m-b-md {
                margin-bottom: 30px;
            }
	  body {
  overflow-x: hidden;
  
}

#sidebar-wrapper {
  min-height: 100vh;
  margin-left: -15rem;
  -webkit-transition: margin .25s ease-out;
  -moz-transition: margin .25s ease-out;
  -o-transition: margin .25s ease-out;
  transition: margin .25s ease-out;
}

#sidebar-wrapper .sidebar-heading {
  padding: 0.875rem 1.25rem;
  font-size: 1.2rem;
}

#sidebar-wrapper .list-group {
  width: 15rem;
}

#page-content-wrapper {
  min-width: 100vw;
}

#wrapper.toggled #sidebar-wrapper {
  margin-left: 0;
}

@media (min-width: 768px) {
  #sidebar-wrapper {
    margin-left: 0;
  }

  #page-content-wrapper {
    min-width: 0;
    width: 100%;
  }

  #wrapper.toggled #sidebar-wrapper {
    margin-left: -15rem;
  }
}
</style>

  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>
@endsection
