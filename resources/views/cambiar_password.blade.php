
 
@extends('layouts.app')

@section('content')

  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="border-right" style="background-color:#4A82C3; position: fixed;height: 100%;  z-index: 3000;" id="sidebar-wrapper">
      <div class="sidebar-heading" style="padding-left:9%;background-color:white;"><img src="descarga.jpg" alt="" width="170px;" height="32px"></div>
      <div class="list-group list-group-flush">
      <a  id="incidencia" href="{{url('/home')}}" class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-exclamation-triangle pr-3"></i>Incidencia</a>
      <a  id="analisiso" href="{{url('/elemento/{id}')}}" class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-chart-area pr-3"></i>Análisis Online</a>
      @if(auth()->user()->rol == 1)
        <a  id="analisis"href="{{url('/grafica')}}"class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-chart-bar pr-3"></i>Análisis</a>
        <a  id="historial"href="{{url('/admin/historial_incidencias')}}"class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-clipboard-list pr-3"></i>Historial de Incidencias</a>
        <a  id="usuarios"href="{{url('/admin/users_list')}}"class="list-group-item list-group-item-action text-white menu" style="background-color:#4A82C3"><i class="fas fa-users text-white mr-2"></i>Usuarios</a>
      @endif             
      </div>
      <a class="list-group-item list-group-item-action text-white menu" style="font-size:15px;background-color:#4A82C3;position:absolute;bottom:0px;justify-content:center">InnovaIT V1.0</a>
    </div>
    <!-- /#sidebar-wrapper -->
  <style>
  .menu:hover{
    background-color:#394880!important;
  }
  </style>

    <!-- Page Content -->
    <div id="page-content-wrapper">

    <nav class="navbar navbar-expand-lg navbar-light  border-bottom" style=" background-color:#4A82C3 ; height: 60.5px; position: fixed;    width: 100%; z-index: 100;">
       

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
          @guest
                            <li class="nav-item">
                                <a class="nav-link text-white" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a id="navbarDropdown" class="nav-link text-white "    aria-haspopup="true" aria-expanded="false" v-pre>
                                    Operario: {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                            </li>
                        @endguest
            <li class="nav-item">
              <a class="nav-link text-white" > {{ date('H:i') }} </a>
            </li>
            <!--<li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Something else here</a>
              </div>
            </li>-->
            <li class="nav-item">
            <a href="{{url('/change_password')}}" class="nav-link"><i class="fas fa-cogs" style="color:white"></i></a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"
              onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
              <i class="fas fa-power-off text-white"></i>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
            </form>
            </li>
          </ul>
        </div>
      </nav>

        <div class="content" style="display: flex;align-items: center;flex-direction: column; justify-content: center; width: 100%;height: 895px; padding-left: 10%;">
        <div class="flex-center position-ref ">
        <div class="container">
    <div class="row justify-content-center">
        
            <div class="card" style="width:500px">
                <div class="card-header text-white" style="display:flex;justify-content:center;align-items:center;background-color:#4A82C3;height:67px;padding-top:4%;">{{ __('Cambiar Contraseña :') }}</div>
                
                <div class="card-body" style="">
                    <form method="POST" action="/cambiar_password/update/{{auth()->user()->id}}">
                        @csrf
                        
                        <div class="form-group row">

                            <label for="usercode" class="col-md-12 col-form-label " style="color:#4A82C3;padding-right: 37%;">{{ __('Nueva contraseña') }}</label>


                            <div class="col-md-8">
                                <input id="password" style="margin-left:30%;" type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" required autocomplete="password" autofocus>

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-12 col-form-label text-md-right" style="color:#4A82C3 ;padding-right: 52%;">{{ __('Repetir Contraseña') }}</label>

                            <div class="col-md-8">
                                <input id="repetir_password" style="margin-left:30%;" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="form-group row mt-1" style="padding-right:24%;">
                            <div class="col-md-8 offset-md-4" style="margin-top:15px">
                                <button type="submit" id="btn" style="background-color:#4A82C3; border: 1px solid #4A82C3;width:200px;" disabled="true" class="btn btn-primary">
                                    {{ __('Cambiar Contraseña') }}
                                </button>
                            </div>
                        </div>
                   
                    </form>
                </div>
            </div>
       
    </div>
</div>
        </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->
  <script>
                        $(document).ready(function(){
                        $("#password").change(function() {
                            $pass=$('#password').val();
                            $r_pass=$('#repetir_password').val();
                            if($pass == $r_pass){
                            $("#btn").attr("disabled",false);
                            }else{
                                $("#btn").attr("disabled",true);
                            }
                        });
                        $("#repetir_password").change(function() {
                            $pass=$('#password').val();
                            $r_pass=$('#repetir_password').val();
                            if($pass == $r_pass){
                            $("#btn").attr("disabled",false);
                            }else{
                                $("#btn").attr("disabled",true);
                            }
                        });
                        });
                        </script>
  <style>
     html, body {
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
                overflow-y:hidden;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }


            .content {
                text-align: center;
                background-color:#EEEEEE;
                width:100%;
                height:189%;
                display:flex;
                align-content:center;
                align-items:center;
                justify-content:center;
                flex-direction:column;
            }


            .formulario {
            border: none;
            padding: 16px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            transition-duration: 0.4s;
            cursor: pointer;
            margin-top:20px;
}
            }
            
            .m-b-md {
                margin-bottom: 30px;
            }
	  body {
  overflow-x: hidden;
  
}

#sidebar-wrapper {
  min-height: 100vh;
  margin-left: -15rem;
  -webkit-transition: margin .25s ease-out;
  -moz-transition: margin .25s ease-out;
  -o-transition: margin .25s ease-out;
  transition: margin .25s ease-out;
}

#sidebar-wrapper .sidebar-heading {
  padding: 0.875rem 1.25rem;
  font-size: 1.2rem;
}

#sidebar-wrapper .list-group {
  width: 15rem;
}

#page-content-wrapper {
  min-width: 100vw;
}

#wrapper.toggled #sidebar-wrapper {
  margin-left: 0;
}

@media (min-width: 768px) {
  #sidebar-wrapper {
    margin-left: 0;
  }

  #page-content-wrapper {
    min-width: 0;
    width: 100%;
  }

  #wrapper.toggled #sidebar-wrapper {
    margin-left: -15rem;
  }
}
</style>

  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>
@stop
